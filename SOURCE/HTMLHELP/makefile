# OpenWatcom Makefile for HTMLHELP
# (C) 2021 - Andy Stamp

# Build standard EXE with:
#   wmake htmlhelp.exe
# Build UPX compressed EXE with:
#   wmake help.exe
# Build a debug EXE with:
# 	wmake clean
#   wmake htmlhelp.exe DEBUG=1


# Delete built files on error.
.ERASE

CC=wcc

# Quiet output.
CCFLAGS=-q
# Generate code for 8086 or higher.
CCFLAGS+= -0
# Compile for DOS target.
CCFLAGS+= -bt=dos
# Use floating point emulation.
CCFLAGS+= -fpc
# Set warning level to max.
CCFLAGS+= -wx
# Set calling convention to __cdecl.
CCFLAGS+= -ecc
# Large memory model.
CCFLAGS+= -ml
# Maximum optimization
CCFLAGS+= -ox
# Place functions in separate segments.
CCFLAGS+= -zm

LINK=wlink

# Build DOS EXE file.
LDFLAGS=system dos
!ifdef DEBUG
# Include debug info
LDFLAGS+= debug dwarf
!endif
# Remove unreferenced sections at link time.
LDFLAGS+= option eliminate
# Generate map file.
LDFLAGS+= option map

UPX=upx

# Work on any 8086.
UPXFLAGS=--8086

# Clear builtin extensions and configure most to least built.
.EXTENSIONS:
.EXTENSIONS: .com .exe .obj .asm .c .h

# Main target.
all: htmlhelp.exe

# Assemble with MASM compatibility.
.asm.obj:
	wasm -zq -zcm=masm $[*

.c: cats;unz;zlib_hh
.c.obj: .AUTODEPEND
	$(CC) $[* $(CCFLAGS)
	
# HELP
OBJS+= events.obj help.obj help_gui.obj help_htm.obj parse.obj pes.obj
OBJS+= prepare.obj readfile.obj search.obj utfcp.obj
OBJS+= hh_conio.obj hh_conio2.obj
# CATS
OBJS+= kitten.obj
# UNZ
OBJS+= ioapi.obj unzip.obj
# ZLIB_HH
OBJS+= adler32.obj crc32.obj inffast.obj inflate.obj inftrees.obj zutil.obj

htmlhelp.exe: $(OBJS)
	*$(LINK) $(LDFLAGS) name $^@ file { $< }

help.exe: htmlhelp.exe
	$(UPX) $(UPXFLAGS) -o $^@ $[@

clean: .SYMBOLIC
	DEL *.OBJ
	DEL *.ERR
	DEL *.EXE
	DEL *.MAP
